This crate is a Bitmessage node implementation as a library for Koibumi, an experimental Bitmessage client.

See [`koibumi`](https://crates.io/crates/koibumi) for more about the application.
See [Bitmessage](https://bitmessage.org/) for more about the protocol.
