# Version 0.0.4 (2021-01-)

* Added `deny(unsafe_code)` directive.

# Version 0.0.3 (2020-09-09)

* Added a bootstrap configuration.

# Version 0.0.2 (2020-09-07)

* Supports aliases for addresses.
* Supports contact list.

# Version 0.0.1 (2020-08-25)

* Code about managing user interfaces to identities.

# Version 0.0.0 (2020-08-17)

* Common code about the inbox/outbox manager.
* Common code about the config file.
* Common code about constants.
* Common code about logging.
* Common code about command line argument parameters.
