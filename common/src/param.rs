//! The parser of the command line parameters.

use std::path::{Path, PathBuf};

/// An object representation of command line argument parameters.
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Params {
    data_dir: PathBuf,
    verbose: bool,
}

impl Params {
    /// Constructs a parameter object from command line arguments.
    pub fn new() -> Self {
        Self::default()
    }

    /// Returns the data directory path.
    pub fn data_dir(&self) -> &Path {
        self.data_dir.as_path()
    }

    /// Returns the verbose flag.
    pub fn verbose(&self) -> bool {
        self.verbose
    }
}

impl Default for Params {
    fn default() -> Self {
        let args: Vec<String> = std::env::args().collect();
        let mut opts = getopts::Options::new();
        opts.optopt("d", "data-dir", "set data directory path", "DIRECTORY");
        opts.optflag("v", "verbose", "show verbose messages");
        let matches = match opts.parse(&args[1..]) {
            Ok(m) => m,
            Err(f) => panic!("{}", f.to_string()),
        };

        let data_dir = if let Some(data_dir) = matches.opt_str("d") {
            Path::new(&data_dir).to_path_buf()
        } else {
            match directories::ProjectDirs::from("onion", "kashiko", "koibumi") {
                Some(project_dirs) => project_dirs.config_dir().to_path_buf(),
                None => panic!("cannot find config directory"),
            }
        };

        let verbose = matches.opt_present("v");

        Self { data_dir, verbose }
    }
}
