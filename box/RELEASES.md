# Version 0.0.4 (2021-01-)

* Supports retrieving time and encoding.
* Added `deny(unsafe_code)` directive.

# Version 0.0.3 (2020-09-09)

* Updated libraries.

# Version 0.0.2 (2020-09-07)

* Redefine database tables.
* Supports aliases for addresses.
* Supports contact list.

# Version 0.0.1 (2020-08-25)

* Stores public keys and private keys.

# Version 0.0.0 (2020-08-17)

* Stores user objects and subscription addressess on the database.
* Stores broadcast messages on the database.
