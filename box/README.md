This crate is an inbox/outbox module for Koibumi, an experimental Bitmessage client.

See [`koibumi`](https://crates.io/crates/koibumi) for more about the application.
See [Bitmessage](https://bitmessage.org/) for more about the protocol.
