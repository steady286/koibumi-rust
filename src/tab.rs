use iced::{button, Button, Column, Length, Row, Text};

use crate::{config::Config as GuiConfig, gui::Message, style};

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Tab {
    Messages,
    Send,
    Identities,
    Contacts,
    Subscriptions,
    Settings,
    Status,
    Log,
}

impl Default for Tab {
    fn default() -> Self {
        Self::Messages
    }
}

#[derive(Clone, Debug, Default)]
pub(crate) struct Tabs {
    messages_button: button::State,
    send_button: button::State,
    identities_button: button::State,
    contacts_button: button::State,
    subscriptions_button: button::State,
    settings_button: button::State,
    status_button: button::State,
    log_button: button::State,
}

impl Tabs {
    pub(crate) fn view(&mut self, config: &GuiConfig, current_tab: Tab) -> Column<Message> {
        let text_size = config.text_size();

        let tab_button = |state, label, tab| {
            Button::new(state, Text::new(label).size(text_size))
                .style(style::TabButton::new(tab == current_tab))
                .on_press(Message::TabSelected(tab))
                .padding(text_size / 6)
        };

        let row1 = Row::new().spacing(text_size / 4).push(
            Row::new()
                .width(Length::Shrink)
                .spacing(text_size / 4)
                .push(tab_button(
                    &mut self.messages_button,
                    "Messages",
                    Tab::Messages,
                ))
                .push(tab_button(&mut self.send_button, "Send", Tab::Send))
                .push(tab_button(
                    &mut self.identities_button,
                    "Identities",
                    Tab::Identities,
                ))
                .push(tab_button(
                    &mut self.contacts_button,
                    "Contacts",
                    Tab::Contacts,
                ))
                .push(tab_button(
                    &mut self.subscriptions_button,
                    "Subscriptions",
                    Tab::Subscriptions,
                )),
        );
        let row2 = Row::new().spacing(text_size / 4).push(
            Row::new()
                .width(Length::Shrink)
                .spacing(text_size / 4)
                .push(tab_button(
                    &mut self.settings_button,
                    "Settings",
                    Tab::Settings,
                ))
                .push(tab_button(&mut self.status_button, "Status", Tab::Status))
                .push(tab_button(&mut self.log_button, "Log", Tab::Log)),
        );
        Column::new().push(row1).push(row2)
    }
}
