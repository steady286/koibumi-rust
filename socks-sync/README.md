This crate is a minimal SOCKS5 client library (sync version).

The library is intended to be used with a local Tor SOCKS5 proxy.
