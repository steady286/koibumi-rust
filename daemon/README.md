# Koibumi daemon

__Koibumi daemon__ is an experimental [Bitmessage](https://bitmessage.org/) client daemon.
Note that Koibumi daemon is __NOT__ an official project of The Bitmessage Developers.

## Features

Koibumi daemon can connect to the Bitmessage network
and relay Bitmessage objects.

Currently, this client can receive chan and broadcast messages.

By default, network connections are limited to __via Tor only__.
In this case, you need a Tor SOCKS5 proxy running at localhost.

The objects loaded from the network and
the list of known node addresses are saved on local file system by using SQLite.
Configurations can be loaded from the file saved by the GUI version of Koibumi.
The data directory can be changed by specifying a `-d` option on the command line.

Koibumi daemon does not have any GUI.
If you need a GUI, use
[`koibumi`](https://crates.io/crates/koibumi) instead.

## Usage

To install the Koibumi Bitmessage client daemon, issue the command:

```sh
cargo install koibumi-daemon
```

To run the client, run `koibumi-daemon` command.

This client is experimental and under development,
so many debug logs are printed on the console.
Adding `-v` option on the command line, more messages are printed.

Note that since database format can be changed among versions,
you may have to remove database files located at `$HOME/.config/koibumi` when trying new version.

## Optional features

If you enable `ctrlc` feature, you can control termination by Ctrl-C key.
To stop gracefully, push Ctrl-C on the console and wait for tasks to exit.
If you can not wait, push Ctrl-C once more to abort tasks.
If they are not aborted, push Ctrl-C yet once more to terminate abruptly.
