//! Object types defined in the Bitmessage protocol.

mod msg;
pub use crate::object::msg::{Msg, TryIntoMsgError};

mod broadcast;
pub use crate::object::broadcast::{
    Broadcast, BroadcastV4, BroadcastV5, Tag, TryIntoBroadcastError,
};

mod onionpeer;
pub use crate::object::onionpeer::{Onionpeer, TryFromOnionpeerError};

use std::{
    convert::TryFrom,
    fmt,
    io::{self, Read, Write},
};

use crate::{
    address::{Address, Error as AddressError},
    crypto::{self, PrivateKeyError},
    io::{LenBm, ReadFrom, WriteTo},
    time::Time,
    var_type::VarInt,
};

pub use crate::stream::StreamNumber;

/// Represents type of object such as public key or one-to-one message.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct ObjectType(u32);

impl ObjectType {
    /// Constructs an object type from a value.
    pub const fn new(value: u32) -> Self {
        Self(value)
    }

    /// Returns the value as `u32`.
    pub fn as_u32(self) -> u32 {
        self.0
    }
}

impl fmt::Display for ObjectType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl From<u32> for ObjectType {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

impl WriteTo for ObjectType {
    fn write_to(&self, w: &mut dyn Write) -> io::Result<()> {
        self.0.write_to(w)
    }
}

impl ReadFrom for ObjectType {
    fn read_from(r: &mut dyn Read) -> io::Result<Self>
    where
        Self: Sized,
    {
        Ok(Self(u32::read_from(r)?))
    }
}

impl LenBm for ObjectType {
    fn len_bm(&self) -> usize {
        self.0.len_bm()
    }
}

/// A version number used in Bitmessage objects.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct ObjectVersion(u64);

impl fmt::Display for ObjectVersion {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl ObjectVersion {
    /// Constructs an object version from a value.
    pub const fn new(value: u64) -> Self {
        Self(value)
    }

    /// Returns the value as `u64`.
    pub fn as_u64(self) -> u64 {
        self.0
    }
}

impl From<u64> for ObjectVersion {
    fn from(value: u64) -> Self {
        Self(value)
    }
}

impl WriteTo for ObjectVersion {
    fn write_to(&self, w: &mut dyn Write) -> io::Result<()> {
        VarInt::new(self.0).write_to(w)
    }
}

impl ReadFrom for ObjectVersion {
    fn read_from(r: &mut dyn Read) -> io::Result<Self>
    where
        Self: Sized,
    {
        Ok(Self(VarInt::read_from(r)?.as_u64()))
    }
}

impl LenBm for ObjectVersion {
    fn len_bm(&self) -> usize {
        VarInt::new(self.0).len_bm()
    }
}

/// The header structure of Bitmessage objects.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Header {
    expires_time: Time,
    object_type: ObjectType,
    version: ObjectVersion,
    stream_number: StreamNumber,
}

impl Header {
    /// Constructs an object header from the specified parameters.
    pub fn new(
        expires_time: Time,
        object_type: ObjectType,
        version: ObjectVersion,
        stream_number: StreamNumber,
    ) -> Self {
        Self {
            expires_time,
            object_type,
            version,
            stream_number,
        }
    }

    /// Returns the expires time.
    pub fn expires_time(&self) -> Time {
        self.expires_time
    }

    /// Returns the object type.
    pub fn object_type(&self) -> ObjectType {
        self.object_type
    }

    /// Returns the object version.
    pub fn version(&self) -> ObjectVersion {
        self.version
    }

    /// Returns the stream number.
    pub fn stream_number(&self) -> StreamNumber {
        self.stream_number
    }
}

impl WriteTo for Header {
    fn write_to(&self, w: &mut dyn Write) -> io::Result<()> {
        self.expires_time.write_to(w)?;
        self.object_type.write_to(w)?;
        self.version.write_to(w)?;
        self.stream_number.write_to(w)?;
        Ok(())
    }
}

impl ReadFrom for Header {
    fn read_from(r: &mut dyn Read) -> io::Result<Self>
    where
        Self: Sized,
    {
        Ok(Self {
            expires_time: Time::read_from(r)?,
            object_type: ObjectType::read_from(r)?,
            version: ObjectVersion::read_from(r)?,
            stream_number: StreamNumber::read_from(r)?,
        })
    }
}

impl LenBm for Header {
    fn len_bm(&self) -> usize {
        self.expires_time.len_bm()
            + self.object_type.len_bm()
            + self.version.len_bm()
            + self.stream_number.len_bm()
    }
}

#[test]
fn test_header_write_to() {
    let test = Header::new(
        0x0123_4567_89ab_cdef.into(),
        2.into(),
        3u64.into(),
        1u32.into(),
    );
    let mut bytes = Vec::new();
    test.write_to(&mut bytes).unwrap();
    let expected = [
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x00, 0x00, 0x00, 0x02, 3, 1,
    ];
    assert_eq!(bytes, expected);
}

#[test]
fn test_header_read_from() {
    use std::io::Cursor;

    let mut bytes = Cursor::new([
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x00, 0x00, 0x00, 0x02, 3, 1,
    ]);
    let test = Header::read_from(&mut bytes).unwrap();
    let expected = Header::new(
        0x0123_4567_89ab_cdef.into(),
        2.into(),
        3u64.into(),
        1u32.into(),
    );
    assert_eq!(test, expected);
}

/// Known types of Bitmessage objects.
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum ObjectKind {
    /// getpubkey object type.
    Getpubkey,
    /// pubkey object type.
    Pubkey,
    /// msg object type.
    Msg,
    /// broadcast object type.
    Broadcast,
    /// onionpeer object type.
    Onionpeer,
    //I2p,
    //Addr,
}

const OBJECT_GETPUBKEY: u32 = 0;
const OBJECT_PUBKEY: u32 = 1;
const OBJECT_MSG: u32 = 2;
const OBJECT_BROADCAST: u32 = 3;
const OBJECT_ONIONPEER: u32 = 0x74_6f72;
//const OBJECT_I2P: u32 = 0x493250;
//const OBJECT_ADDR: u32 = 0x61646472;

/// The error type returned when a conversion from a Bitmessage object type
/// to a known object type fails.
///
/// This error is used as the error type for the `TryFrom` implementation
/// for `ObjectKind`.
///
/// The source object type is returned as a payload of this error type.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct TryFromObjectTypeError(ObjectType);

impl fmt::Display for TryFromObjectTypeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unknown object type {}", self.0)
    }
}

impl std::error::Error for TryFromObjectTypeError {}

impl TryFrom<ObjectType> for ObjectKind {
    type Error = TryFromObjectTypeError;

    fn try_from(t: ObjectType) -> Result<Self, <Self as TryFrom<ObjectType>>::Error> {
        match t.as_u32() {
            OBJECT_GETPUBKEY => Ok(Self::Getpubkey),
            OBJECT_PUBKEY => Ok(Self::Pubkey),
            OBJECT_MSG => Ok(Self::Msg),
            OBJECT_BROADCAST => Ok(Self::Broadcast),
            OBJECT_ONIONPEER => Ok(Self::Onionpeer),
            _ => Err(TryFromObjectTypeError(t)),
        }
    }
}

impl From<ObjectKind> for ObjectType {
    fn from(kind: ObjectKind) -> Self {
        match kind {
            ObjectKind::Getpubkey => OBJECT_GETPUBKEY.into(),
            ObjectKind::Pubkey => OBJECT_PUBKEY.into(),
            ObjectKind::Msg => OBJECT_MSG.into(),
            ObjectKind::Broadcast => OBJECT_BROADCAST.into(),
            ObjectKind::Onionpeer => OBJECT_ONIONPEER.into(),
        }
    }
}

/// This error indicates
/// that the decryption failed.
#[derive(Debug)]
pub enum DecryptError {
    /// A standard I/O error was caught during decrypting a message.
    /// The actual error caught is returned as a payload of this variant.
    IoError(io::Error),
    /// Indicates that the operation on a private key failed.
    PrivateKeyError(PrivateKeyError),
    /// A decryption error was caught during decrypting a message.
    /// The actual error caught is returned as a payload of this variant.
    DecryptError(crypto::DecryptError),
    /// Indicates that the stream numbers from the header and the content does not match.
    StreamsNotMatch {
        /// The stream number from the header.
        headers: StreamNumber,
        /// The stream number from the content.
        contents: StreamNumber,
    },
    /// An error was caught during conversion from public keys to an address.
    /// The actual error caught is returned as a payload of this variant.
    AddressError(AddressError),
    /// Indicates that the address reconstructed from the message content does not match the sender address.
    /// The expected and the actual addressess are returned as payloads of this variant.
    InvalidAddress {
        /// The expected address.
        expected: Address,
        /// The actual address.
        actual: Address,
    },
    /// An error was caught during verification of the signature.
    /// The actual error caught is returned as a payload of this variant.
    VerifyError(crypto::VerifyError),
}

impl fmt::Display for DecryptError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::IoError(err) => err.fmt(f),
            Self::PrivateKeyError(err) => err.fmt(f),
            Self::DecryptError(err) => err.fmt(f),
            Self::StreamsNotMatch { headers, contents } => write!(
                f,
                "streams not match: header's is {}, but content's is {}",
                headers, contents
            ),
            Self::AddressError(err) => err.fmt(f),
            Self::InvalidAddress { expected, actual } => write!(
                f,
                "address is expected to be {}, but actual is {}",
                expected, actual
            ),
            Self::VerifyError(err) => err.fmt(f),
        }
    }
}

impl std::error::Error for DecryptError {}

impl From<io::Error> for DecryptError {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}

impl From<PrivateKeyError> for DecryptError {
    fn from(err: PrivateKeyError) -> Self {
        Self::PrivateKeyError(err)
    }
}

impl From<crypto::DecryptError> for DecryptError {
    fn from(err: crypto::DecryptError) -> Self {
        Self::DecryptError(err)
    }
}

impl From<AddressError> for DecryptError {
    fn from(err: AddressError) -> Self {
        Self::AddressError(err)
    }
}

impl From<crypto::VerifyError> for DecryptError {
    fn from(err: crypto::VerifyError) -> Self {
        Self::VerifyError(err)
    }
}
