//! Types on SOCKS.

// See RFC 1928 SOCKS Protocol Version 5

use std::{
    fmt,
    net::{Ipv4Addr, Ipv6Addr, SocketAddrV4, SocketAddrV6},
    num::ParseIntError,
    str::FromStr,
};

use crate::{
    domain::{Domain, ParseDomainError, SocketDomain},
    port::Port,
};

/// Represents IPv4, IPv6 address or a domain name used by SOCKS5.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum Addr {
    /// An IPv4 address.
    Ipv4(Ipv4Addr),

    /// A domain name.
    Domain(Domain),

    /// An IPv6 address.
    Ipv6(Ipv6Addr),
}

impl fmt::Display for Addr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Addr::Ipv4(addr) => addr.fmt(f),
            Addr::Domain(domain) => domain.fmt(f),
            Addr::Ipv6(addr) => addr.fmt(f),
        }
    }
}

impl FromStr for Addr {
    type Err = ParseAddrError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(addr) = s.parse() {
            return Ok(Self::Ipv4(addr));
        }
        if let Ok(addr) = s.parse() {
            return Ok(Self::Ipv6(addr));
        }
        Ok(Self::Domain(Domain::new(s)?))
    }
}

/// An error which can be returned when parsing a SOCKS address
/// or a SOCKS socket address.
///
/// This error is used as the error type for the `FromStr` implementation
/// for [`Addr`].
/// This error is also used as a variant of the error type
/// [`ParseSocketAddrError`].
///
/// [`Addr`]: enum.Addr.html
/// [`ParseSocketAddrError`]: enum.ParseSocketAddrError.html
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ParseAddrError {
    /// Failed when parsing the input as a domain name.
    /// The actual error caught is returned as a payload of this variant.
    ParseDomainError(ParseDomainError),
}

impl fmt::Display for ParseAddrError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::ParseDomainError(err) => err.fmt(f),
        }
    }
}

impl From<ParseDomainError> for ParseAddrError {
    fn from(err: ParseDomainError) -> Self {
        Self::ParseDomainError(err)
    }
}

impl std::error::Error for ParseAddrError {}

/// Represents a socket address used by SOCKS5.
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum SocketAddr {
    /// A socket address using an IPv4 address.
    Ipv4(SocketAddrV4),

    /// A socket address using a domain name.
    Domain(SocketDomain),

    /// A socket address using an IPv6 address.
    Ipv6(SocketAddrV6),
}

impl SocketAddr {
    /// Constructs a new socket address from an address with a port number.
    pub fn new(addr: Addr, port: Port) -> Self {
        match addr {
            Addr::Ipv4(addr) => Self::Ipv4(SocketAddrV4::new(addr, port.as_u16())),
            Addr::Domain(domain) => Self::Domain(SocketDomain::new(domain, port)),
            Addr::Ipv6(addr) => Self::Ipv6(SocketAddrV6::new(addr, port.as_u16(), 0, 0)),
        }
    }
}

impl fmt::Display for SocketAddr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SocketAddr::Ipv4(addr) => addr.fmt(f),
            SocketAddr::Domain(domain) => domain.fmt(f),
            SocketAddr::Ipv6(addr) => addr.fmt(f),
        }
    }
}

impl FromStr for SocketAddr {
    type Err = ParseSocketAddrError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let colon = s.rfind(':');
        if colon.is_none() {
            return Err(Self::Err::PortNotFound);
        }
        let colon = colon.unwrap();

        let mut addr_part = &s[..colon];
        if addr_part.starts_with('[') && addr_part.ends_with(']') {
            addr_part = &addr_part[1..addr_part.len() - 1];
        }
        let port_part = &s[colon + 1..];

        let port = port_part.parse::<Port>()?;
        let addr = addr_part.parse()?;
        Ok(Self::new(addr, port))
    }
}

impl From<SocketDomain> for SocketAddr {
    fn from(domain: SocketDomain) -> Self {
        Self::Domain(domain)
    }
}

/// An error which can be returned when parsing a SOCKS socket address.
///
/// This error is used as the error type for the `FromStr` implementation
/// for [`SocketAddr`].
///
/// [`SocketAddr`]: enum.SocketAddr.html
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ParseSocketAddrError {
    /// The input did not have any port number.
    PortNotFound,

    /// An error was caught when parsing a port number.
    /// The actual error caught is returned as a payload of this variant.
    InvalidPort(ParseIntError),

    /// An error was caught when parsing
    /// an extended Bitmessage network address.
    /// The actual error caught is returned as a payload of this variant.
    InvalidAddr(ParseAddrError),
}

impl fmt::Display for ParseSocketAddrError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::PortNotFound => write!(f, "port not found"),
            Self::InvalidPort(err) => err.fmt(f),
            Self::InvalidAddr(err) => err.fmt(f),
        }
    }
}

impl std::error::Error for ParseSocketAddrError {}

impl From<ParseIntError> for ParseSocketAddrError {
    fn from(err: ParseIntError) -> Self {
        Self::InvalidPort(err)
    }
}

impl From<ParseAddrError> for ParseSocketAddrError {
    fn from(err: ParseAddrError) -> Self {
        Self::InvalidAddr(err)
    }
}

#[test]
fn test_socket_addr_from_str() {
    let test = "example.org:1234".parse::<SocketAddr>().unwrap();
    let domain = SocketDomain::new(Domain::new("example.org").unwrap(), 1234.into());
    let expected = SocketAddr::Domain(domain);
    assert_eq!(test, expected);
}
