//! Provides a simple logger.

use log::{Level, LevelFilter, Metadata, Record, SetLoggerError};

use crate::param::Params;

/// A simple logger that filters log messages and prints them to stdout.
/// The log messages whose target string starts with "koibumi" are exclusively printed.
struct SimpleLogger;

impl log::Log for SimpleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        if metadata.level() <= Level::Warn {
            return true;
        }
        if metadata.target().starts_with("koibumi") {
            return metadata.level() <= Level::Trace;
        }
        false
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("{} - {}", record.level(), record.args());
        }
    }

    fn flush(&self) {}
}

/// The global logger object.
static LOGGER: SimpleLogger = SimpleLogger;

/// Initializes a logger.
pub fn init(params: &Params) -> Result<(), SetLoggerError> {
    if params.verbose() {
        log::set_logger(&LOGGER).map(|()| log::set_max_level(LevelFilter::Trace))
    } else {
        log::set_logger(&LOGGER).map(|()| log::set_max_level(LevelFilter::Info))
    }
}
