use std::{path::PathBuf, sync::Arc, thread};

use crossbeam_channel::{bounded, Receiver, Sender};
use log::error;

use koibumi_core::{
    address::Address,
    identity::Private as PrivateIdentity,
    message::{self, UserAgent},
    object,
};

use crate::{
    config::Config,
    connection_loop::{ConnectionManager, Context},
    net::SocketAddrNode,
    node_manager::Rating,
    user_manager::User,
};

/// The events which occur in a Bitmessage node.
#[derive(Clone, Debug)]
pub enum Event {
    /// Indicates the stats of the counts of connections have been changed.
    ConnectionCounts {
        /// The count of incoming initiated connections.
        incoming_initiated: usize,
        /// The count of incoming connected connections.
        incoming_connected: usize,
        /// The count of incoming established connections.
        incoming_established: usize,
        /// The count of outgoing initiated connections.
        outgoing_initiated: usize,
        /// The count of outgoing connected connections.
        outgoing_connected: usize,
        /// The count of outgoing established connections.
        outgoing_established: usize,
    },

    /// Indicates the count of known node addresses has been changed.
    AddrCount(usize),

    /// Indicates a connection to a node has newly been established.
    /// The socket address, the user agent
    /// and the rating of the connectivity of the node are returned.
    Established {
        /// The socket address.
        addr: SocketAddrNode,
        /// The user agent.
        user_agent: UserAgent,
        /// The rating.
        rating: Rating,
    },

    /// Indicates an established connection to a node has been disconnected.
    /// The socket address of the node is returned.
    Disconnected {
        /// The socket address.
        addr: SocketAddrNode,
    },

    /// Indicates the stats of the counts of objects are changed.
    Objects {
        /// The count of missing objects.
        missing: usize,
        /// The count of loaded objects.
        loaded: usize,
        /// The count of uploaded objects.
        uploaded: usize,
    },

    /// Indicates the node has been stopped.
    Stopped,

    /// Indicates that an user received a msg message.
    Msg {
        /// The user ID.
        user_id: Vec<u8>,
        /// The Bitmessage address of the receiver of the msg message.
        address: Address,
        /// The message object.
        object: message::Object,
    },

    /// Indicates that an user received a broadcast message.
    Broadcast {
        /// The user ID.
        user_id: Vec<u8>,
        /// The Bitmessage address of the sender of the broadcast message.
        address: Address,
        /// The message object.
        object: message::Object,
    },
}

/// The commands which accepted by a node.
pub enum Command {
    /// Initializes the node with specified configuration set
    /// and starts the various background tasks
    /// with specified database connection pool.
    /// No outgoing connections are initiated yet.
    /// Incoming connections can be accepted.
    ///
    /// `Response::Started` with event receiver will be returned.
    Start(Box<Config>, PathBuf, Vec<User>),

    /// Disconnects all connections and stop the node.
    Stop,

    /// Abort the tasks which remain after stop command was issued.
    Abort,

    /// Performs PoW and sends the object.
    Send {
        /// The header of the object to send.
        header: object::Header,
        /// The payload of the object to send.
        payload: Vec<u8>,
    },

    /// Adds an identity to the user specified by the ID.
    AddIdentity {
        /// The user ID.
        id: Vec<u8>,
        /// The identity.
        identity: PrivateIdentity,
    },

    /// Makes the user subscribe to the address.
    Subscribe {
        /// The user ID.
        id: Vec<u8>,
        /// The address.
        address: Address,
    },
}

/// The responses to the commands.
pub enum Response {
    /// Indicates the node has been started.
    /// The receiver for events is returned.
    Started(Receiver<Event>),
}

/// Runs an event loop that manage a Bitmessage node.
pub fn run(receiver: Receiver<Command>, sender: Sender<Response>) {
    let receiver = receiver;
    let sender = sender;

    let mut conn_mngr: Option<ConnectionManager> = None;
    let mut ctx: Option<Arc<Context>> = None;
    let mut _handle: Option<thread::JoinHandle<()>> = None;
    while let Ok(event) = receiver.recv() {
        match event {
            Command::Start(config, db_path, users) => {
                if conn_mngr.is_none() {
                    let (bm_event_sender, bm_event_receiver) = bounded(config.channel_buffer());
                    let cm = ConnectionManager::new(
                        config.as_ref().clone(),
                        db_path,
                        users,
                        bm_event_sender,
                    );
                    sender.send(Response::Started(bm_event_receiver)).unwrap();
                    ctx = Some(cm.ctx());
                    conn_mngr = Some(cm);
                }
            }
            Command::Stop => {
                if let Some(cm) = conn_mngr {
                    _handle = Some(cm.stop());
                    conn_mngr = None;
                }
            }
            Command::Abort => {
                if let Some(s) = ctx {
                    s.abort();
                    ctx = None;
                }
            }
            Command::Send { header, payload } => {
                if let Some(cm) = &conn_mngr {
                    if let Err(err) = cm.send(header, payload) {
                        error!("{}", err);
                    }
                }
            }
            Command::AddIdentity { id, identity } => {
                if let Some(cm) = &conn_mngr {
                    if let Err(err) = cm.add_identity(id, identity) {
                        error!("{}", err);
                    }
                }
            }
            Command::Subscribe { id, address } => {
                if let Some(cm) = &conn_mngr {
                    if let Err(err) = cm.subscribe(id, address) {
                        error!("{}", err);
                    }
                }
            }
        }
    }
}
