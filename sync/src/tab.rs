use conrod_core::{color, text, widget, Colorable, Labelable, Positionable, Sizeable, Widget};

use crate::{config::Config as GuiConfig, ids::Ids};

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Tab {
    Messages,
    Send,
    Identities,
    Contacts,
    Subscriptions,
    Settings,
    Status,
    Log,
}

impl Default for Tab {
    fn default() -> Self {
        Self::Messages
    }
}

#[derive(Clone, Debug, Default)]
pub(crate) struct Tabs {
    selected: Tab,
}

impl Tabs {
    pub(crate) fn selected(&self) -> Tab {
        self.selected
    }

    pub(crate) fn set_widgets(
        &mut self,
        ui: &mut conrod_core::UiCell,
        ids: &mut Ids,
        config: &GuiConfig,
    ) {
        let text_size = config.text_size();

        widget::Canvas::new()
            .flow_down(&[
                (
                    ids.tabs_header,
                    widget::Canvas::new()
                        .length((text_size as f64 + 8.0 + 2.0) * 2.0 + 2.0)
                        .pad_bottom(2.0),
                ),
                (ids.tabs_body, widget::Canvas::new()),
            ])
            .top_left_of(ids.body)
            .wh_of(ids.body)
            .color(color::DARK_BLUE)
            .set(ids.tabs, ui);

        let selected = self.selected;
        let button = |ui: &mut conrod_core::UiCell, label, tab| {
            let mut width = 64.0;
            for font_id in ui.fonts.ids() {
                width = text::line::width(label, ui.fonts.get(font_id).unwrap(), text_size as u32);
            }
            let c = if selected == tab {
                (color::LIGHT_GREEN, color::WHITE)
            } else {
                (color::DARK_GREEN, color::GRAY)
            };
            widget::Button::new()
                .w_h(width + 8.0, text_size as f64 + 8.0)
                .label_font_size(text_size as u32)
                .label(label)
                .color(c.0)
                .label_color(c.1)
        };

        for _click in button(ui, "Messages", Tab::Messages)
            .top_left_with_margins_on(ids.tabs_header, 2.0, 2.0)
            .set(ids.tabs_button_messages, ui)
        {
            self.selected = Tab::Messages;
        }

        for _click in button(ui, "Send", Tab::Send)
            .right_from(ids.tabs_button_messages, 2.0)
            .set(ids.tabs_button_send, ui)
        {
            self.selected = Tab::Send;
        }

        for _click in button(ui, "Identities", Tab::Identities)
            .right_from(ids.tabs_button_send, 2.0)
            .set(ids.tabs_button_identities, ui)
        {
            self.selected = Tab::Identities;
        }

        for _click in button(ui, "Contacts", Tab::Contacts)
            .right_from(ids.tabs_button_identities, 2.0)
            .set(ids.tabs_button_contacts, ui)
        {
            self.selected = Tab::Contacts;
        }

        for _click in button(ui, "Subscriptions", Tab::Subscriptions)
            .right_from(ids.tabs_button_contacts, 2.0)
            .set(ids.tabs_button_subscriptions, ui)
        {
            self.selected = Tab::Subscriptions;
        }

        for _click in button(ui, "Settings", Tab::Settings)
            .down_from(ids.tabs_button_messages, 2.0)
            .set(ids.tabs_button_settings, ui)
        {
            self.selected = Tab::Settings;
        }

        for _click in button(ui, "Status", Tab::Status)
            .right_from(ids.tabs_button_settings, 2.0)
            .set(ids.tabs_button_status, ui)
        {
            self.selected = Tab::Status;
        }

        for _click in button(ui, "Log", Tab::Log)
            .right_from(ids.tabs_button_status, 2.0)
            .set(ids.tabs_button_log, ui)
        {
            self.selected = Tab::Log;
        }
    }
}
