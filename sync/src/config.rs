use serde::{Deserialize, Serialize};

/// A set of configurations for the Koibumi Bitmessage GUI.
#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct Config {
    #[serde(default = "default_text_size")]
    text_size: u16,
}

fn default_text_size() -> u16 {
    12
}

impl Default for Config {
    fn default() -> Self {
        Self {
            text_size: default_text_size(),
        }
    }
}

impl Config {
    pub fn text_size(&self) -> u16 {
        self.text_size
    }
}
